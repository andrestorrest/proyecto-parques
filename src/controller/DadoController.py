import random
from tkinter import PhotoImage

from src.model import Posicion
from src.model.Dado import Dado 

class DadoController:
    def __init__(self):
        self.posicionDado1=Posicion(1055,520)
        self.posicionDado2=Posicion(1115,520)
    def generarImagen(self):
        return PhotoImage(file = f"Recursos/dado{Dado.get_Numero()}.png")