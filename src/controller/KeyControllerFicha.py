from pynput import keyboard

class KeyControllerFicha:

    def __init__(self):
        self._posicion = None
        self._ficha = None

    def set_position(self,posicion):
        self._posicion = posicion

    def set_ficha(self,ficha):
        del self._ficha
        self._ficha = ficha

    
    def mover_flecha_ficha(self,key):
        if(key==keyboard.Key.up):
            self._posicion.set_y(self._posicion.get_y() - 5)
        elif(key==keyboard.Key.down):
            self._posicion.set_y(self._posicion.get_y() + 5)
        elif(key==keyboard.Key.right):
            self._posicion.set_x(self._posicion.get_x() + 5)
        elif(key==keyboard.Key.left):
            self._posicion.set_x(self._posicion.get_x() - 5)
        self._ficha.place(x=self._posicion.get_x(),y=self._posicion.get_y())