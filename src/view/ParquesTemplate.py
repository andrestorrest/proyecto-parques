from tkinter import ttk 
from tkinter import *
from pynput import keyboard
from src.controller.DadoController import DadoController
from src.model import Posicion
import random

from src.controller.KeyControllerFicha import KeyControllerFicha

class ParquesTemplate():

    #variables de coodernadas

    posicionF1=Posicion(453,460)
    posicionF2=Posicion(485,460)
    posicionF3=Posicion(517,460)
    posicionF4=Posicion(550,460)

    carcelF1=True
    carcelF2=True
    carcelF3=True
    carcelF4=True

    def __init__(self):

        self.keyController = KeyControllerFicha()
        self.dadoController= DadoController()
        
        #create window
        self.tablero= Tk()
        self.tablero.title("Tablero")
        self.tablero.geometry("1200x600")
        self.tablero.resizable(False,False)
        
        # Create fondo madera
        self.fondoMadera = Frame(self.tablero)
        self.fondoMadera.pack(fill = "both", expand = True)
        
        # Add image file
        maderaFondo = PhotoImage(file = "Recursos/madera.png")
        
        # Show image using label
        eMadera = Label(self.fondoMadera, image = maderaFondo)
        eMadera.place(x = 0,y = 0)
        
        # Create fondo tablero
        self.fondoTablero = Frame(self.fondoMadera)
        self.fondoTablero.config(width=800,height=600)
        self.fondoTablero.place(x=200,y=0)
        
        # Add image file
        tableroFondo= PhotoImage(file = "Recursos/Parques.png")
        
        # Show image using label
        eTablero = Label(self.fondoTablero, image = tableroFondo)
        eTablero.place(x = 0,y = 0)

        # Add image file
        fichaAImage= PhotoImage(file = "Recursos/FA.png")
        
        # Show image using label Fichas Amarillas 
        self.ficha1A = Label(self.fondoTablero, image = fichaAImage,width=30,height=30)
        self.ficha1A.place(x=575,y=440)
        self.ficha2A = Label(self.fondoTablero, image = fichaAImage,width=30,height=30)
        self.ficha2A.place(x=635,y=440)
        self.ficha3A = Label(self.fondoTablero, image = fichaAImage,width=30,height=30)
        self.ficha3A.place(x=690,y=440)
        self.ficha4A = Label(self.fondoTablero, image = fichaAImage,width=30,height=30)
        self.ficha4A.place(x=750,y=440)

        # Add image file
        fichaAZImage= PhotoImage(file = "Recursos/FAZ.png")

        # Show image using label Fichas Azules 
        self.ficha1AZ = Label(self.fondoTablero, image = fichaAZImage,width=30,height=30)
        self.ficha1AZ.place(x=575,y=140)
        self.ficha2AZ = Label(self.fondoTablero, image = fichaAZImage,width=30,height=30)
        self.ficha2AZ.place(x=635,y=140)
        self.ficha3AZ = Label(self.fondoTablero, image = fichaAZImage,width=30,height=30)
        self.ficha3AZ.place(x=690,y=140)
        self.ficha4AZ = Label(self.fondoTablero, image = fichaAZImage,width=30,height=30)
        self.ficha4AZ.place(x=750,y=140)

        # Add image file
        fichaVImage= PhotoImage(file = "Recursos/FV.png")

        # Show image using label Fichas Verdes 
        self.ficha1V = Label(self.fondoTablero, image = fichaVImage,width=30,height=30)
        self.ficha1V.place(x=20,y=440)
        self.ficha2V = Label(self.fondoTablero, image = fichaVImage,width=30,height=30)
        self.ficha2V.place(x=80,y=440)
        self.ficha3V = Label(self.fondoTablero, image = fichaVImage,width=30,height=30)
        self.ficha3V.place(x=140,y=440)
        self.ficha4V = Label(self.fondoTablero, image = fichaVImage,width=30,height=30)
        self.ficha4V.place(x=200,y=440)

        # Add image file
        fichaRImage= PhotoImage(file = "Recursos/FR.png")

        # Show image using label Fichas Rojas
        self.ficha1R = Label(self.fondoTablero, image = fichaRImage,width=30,height=30)
        self.ficha1R.place(x=20,y=140)
        self.ficha2R = Label(self.fondoTablero, image = fichaRImage,width=30,height=30)
        self.ficha2R.place(x=80,y=140)
        self.ficha3R = Label(self.fondoTablero, image = fichaRImage,width=30,height=30)
        self.ficha3R.place(x=140,y=140)
        self.ficha4R = Label(self.fondoTablero, image = fichaRImage,width=30,height=30)
        self.ficha4R.place(x=200,y=140)

        # Create Buttons
        bMoverF1 = Button( self.fondoMadera, text = "Mover Ficha 1",command= self.moverFicha1)
        bMoverF1.place(x=1050,y=100)
        bMoverF2 = Button( self.fondoMadera, text = "Mover Ficha 2",command= self.moverFicha2)
        bMoverF2.place(x=1050,y=150)
        bMoverF3 = Button( self.fondoMadera, text = "Mover Ficha 3",command= self.moverFicha3)
        bMoverF3.place(x=1050,y=200)
        bMoverF4 = Button( self.fondoMadera, text = "Mover Ficha 4",command= self.moverFicha4)
        bMoverF4.place(x=1050,y=250) 
        bFinalizarT = Button( self.fondoMadera, text = "Finalizar Turno")
        bFinalizarT.place(x=1050,y=300) 

        # Add image file
        dado= PhotoImage(file = "Recursos/dados.png")
        #boton dado
        Tirar = Button(self.fondoMadera, text = "Tirar Dados",image=dado,command=self.tirarDados)
        Tirar.place(x=1075,y=380) 

        self.listener = None

        # ejecutar tablero
        self.tablero.mainloop()
     #funciones mover con un click y activacion del listener
    def moverFicha1(self):
        if self.carcelF1:
            self.ficha1A.place(x=self.posicionF1.get_x(),y=self.posicionF1.get_y())
            self.carcelF1=False
        self.listener = keyboard.Listener(on_press=self.moverFlechaFicha1)
        self.keyController.set_position(self.posicionF1)
        self.keyController.set_ficha(self.ficha1A)
        self.listener.start()
        
    def moverFicha2(self):
        if self.carcelF2:
            self.ficha2A.place(x=self.posicionF2.get_x(),y=self.posicionF2.get_y())
            self.carcelF2=False
        self.listener = keyboard.Listener(on_press=self.moverFlechaFicha2)
        self.keyController.set_position(self.posicionF2)
        self.keyController.set_ficha(self.ficha2A)
        self.listener.start()
    
    def moverFicha3(self):
        if self.carcelF3:
            self.ficha3A.place(x=self.posicionF3.get_x(),y=self.posicionF3.get_y())
            self.carcelF3=False
        self.listener = keyboard.Listener(on_press=self.moverFlechaFicha3)
        self.keyController.set_position(self.posicionF3)
        self.keyController.set_ficha(self.ficha3A)
        self.listener.start()

    def moverFicha4(self):
        if self.carcelF4:
            self.ficha4A.place(x=self.posicionF4.get_x(),y=self.posicionF4.get_y())
            self.carcelF4=False
        self.listener = keyboard.Listener(on_press=self.moverFlechaFicha4)
        self.keyController.set_position(self.posicionF4)
        self.keyController.set_ficha(self.ficha4A)
        self.listener.start()

    #funciones de mover con las flechas
    def moverFlechaFicha1(self,key):
        self.keyController.mover_flecha_ficha(key)
    
    def moverFlechaFicha2(self,key):
        self.keyController.mover_flecha_ficha(key)

    def moverFlechaFicha3(self,key):
        self.keyController.mover_flecha_ficha(key)
    
    def moverFlechaFicha4(self,key):
        self.keyController.mover_flecha_ficha(key)
    
     #funcion tirar dados

    def tirarDados(self):
        self.image1 = self.dadoController.generarImagen()
        self.image2 = self.dadoController.generarImagen()
        dado1 = Label(self.fondoMadera, image = self.image1,width=50,height=50)
        dado1.place(x=self.dadoController.posicionDado1.get_x(),
        y=self.dadoController.posicionDado1.get_y())
        dado2 = Label(self.fondoMadera, image = self.image2,width=50,height=50)
        dado2.place(x=self.dadoController.posicionDado2.get_x(),
        y=self.dadoController.posicionDado2.get_y())
