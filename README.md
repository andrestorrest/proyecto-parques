# Proyecto Parques

## Tablero Inicial 
![1](/uploads/21306132d7961dc2575266e6e303ce5d/1.png)

## Salida de Fichas con dados pares 
![2](/uploads/ca57620fb943c2a3a52d7ff6a7f72d35/2.png)

## Movimiento de Fichas por casillas  
![3](/uploads/cf487add213f99355d8761677afe8562/3.png)
